# Gleam Syntax

This syntax reference is a minimal introduction to:

- [Comments](#comments)
- [Literals](#literals)
- [Lists](#lists)
- [Tuples](#tuples)
- [Case Expressions](#case-expressions)
- [Custom Types](#custom-types)
- [Functions](#functions)
- [Operators](#operators)
- [Variable Bindings](#variable-bindings)
- [Modules](#modules)
- [Constants](#constants)
- [Type Annotations](#type-annotations)
- [Type Aliases](#type-aliases)
- [Erlang Interop](#erlang-interop)

Check out [The Official Book](https://gleam.run/book/) for more information.<br>
You can find the source for this document [here](https://gitlab.com/greggreg/gleam-syntax).<br>
This document is hosted at [gleam.fyi](https://gleam.fyi)

### Comments
Anything after two slashes `//` until the end of a line is ignored.
```gleam
// a single line comment
```

Learn more about Comments [here](https://gleam.run/book/tour/comments).

### Literals

```gleam
True  : Bool
False : Bool

42    : Int
2_000 : Int
3.14  : Float

"abc"    : String
<<4:8>>  : BitString

// multi-line String
"
All strings can be multi-
line and you must escape \"quotes\".
"
```

Typical manipulation of literals:

```gleam
True && {True || False}
{2 + 4} * {1_000 - 20}
2.45 + 7.38
```

Gleam also has hex, octal and binary Integer literals
```gleam
0x2A : Int (hexadecimal for 42)
0o52 : Int (octal for 42)
0b101010 : Int (binary for 42)
```

Learn more about Literals: [Numbers](https://gleam.run/book/tour/ints-and-floats), [Strings](https://gleam.run/book/tour/strings)
[Booleans](https://gleam.run/book/tour/bools), [BitStrings](https://gleam.run/book/tour/bit-strings).

### Lists

Here are three things that are equivalent:

```gleam
[1,2,3,4]
[1, ..[2,3,4]]
[1, ..[2, ..[3, ..[4, ..[]]]]]
```
Learn more about Lists [here](https://gleam.run/book/tour/lists).

### Tuples

```gleam
tuple(1, "hi", False)
```
Learn more about Tuples [here](https://gleam.run/book/tour/tuples).

### Case Expressions

```gleam
case power_level {
  x if x > 9000 -> "OVER 9000!!!"
  _ -> "meh"
}
```

You can have multiple values in a case expression if you need.

```gleam
case power_level, is_lazor_ready {
  9000, True -> fire_ma_lazor()
  _, _ -> wait()
}
```

You can also have conditional behavior based on the structure of custom types and literals:

```gleam
case result_list {
  Ok(xs) -> xs
  Error(_) -> []
}

case xs {
  [] ->
    Error(Nil)
    
  [first, ..rest] ->
    Ok(tuple(first, rest))
}

case n {
  0 -> 1
  1 -> 1
  _ -> fib(n-1) + fib(n-2)
}
```
Learn more about Case Expressions [here](https://gleam.run/book/tour/case-expressions).

###  Custom Types

```gleam
// create a Custom Type
type Point {
  Point(x: Int, y: Int, z: Int)
}

let point = Point( x: 3, y: 4, z: 5 )

// access fields
point.x == 3

// update a field
let new_point = Point(..point, x = 1)

// update many fields
let new_point = Point(..point, x = 1, z = 6)
```
Learn more about Custom Types [here](https://gleam.run/book/tour/custom-types.html).

### Functions

```gleam
fn square(n) {
  n *. n
}

fn hypotenuse(a, b) {
  float.square_root(square(a) +. square(b))
}

distance(a,b,x,y) {
  hypotenuse({a -. x}, {b -. y})
}
```

Anonymous functions:

```gleam
fn squares() {
  list.map(list.range(1, 100), fn(n) { n * n })
}
```
Learn more about Functions [here](https://gleam.run/book/tour/functions).

### Operators

Gleam has operators for **Integers**: `+`, `-`, `*`, `/`, and `%`

Operators for **Floats**: `+.`, `-.`, `*.`, `/.`

And the pipe `|>` operator which is for function application
```gleam
// without pipes
fn viewNames1(names) {
  string.join((list.sort(names)), ", ")
}

// with pipes
fn viewNames(names) {
  names
    |> List.sort()
    |> String.join(", ")
}
```
By default `|>` uses its left hand side as the first argument to it's right hand side, but you can choose which argument to fill in by using the `_` character.
```gleam
fn add_unit(unit, value) {
  case unit {
    "$" -> "$" |> string.append(_, value)
    _ -> unit |> string.append(value, _)
  }
}
```

Learn more about `|>` [here](https://gleam.run/book/tour/functions.html#pipe-operator).


### Variable Bindings

```gleam
{
  let twenty_four = 3 * 8
  let sixteen = 4 + 12
  twenty_four + sixteen
}
```
`let` takes a pattern on its left side so you can bind to a sub structure. You can ignore part of a structure with `_`.

```gleam
{
  let tuple( three, four, _ ) = tuple( 3, 4, 5 )
  let hypotenuse = fn(a, b)) { float.square_root(a*a + b*b) }
  hypotenuse(three, four)
}
```
You can add type annotations in let-expressions.

```gleam
{
  let name: String = "Hermann"
  let increment: fn(Int) -> Int = fn(n) { n + 1 }
  increment(10)
}
```

Learn more about Bindings [here](https://gleam.run/book/tour/let-bindings).

### Modules
Module names must match their file name, so the module `parser/utils` needs to be in the file `parser/utils.gleam`.

```gleam
// qualified imports
import gleam/list                            // list.map, list.fold
import gleam/list as l                       // l.map, l.fold

// unqualified imports
import gleam/list.{map, fold}                // map, fold

import gleam/option.{ Option }               // the type Option
import gleam/option.{ Option, Some, None }   // type: Option, constructors: Some & None
```

Qualified imports are preferred.

Learn more about Modules [here](https://gleam.run/book/tour/modules.html)

### Constants
Literal values can be used as constants.

```gleam
const location = "Hill Valley"
const year = 1955

fn at_88() {
 travel(location, year)
}
```

Learn more about Constants [here](https://gleam.run/book/tour/constants.html)

### Type Annotations

```gleam
answer: Int = 42

fn factorial(n: Int) -> Int {
  list.fold(List.range(1, n), 1, fn(n, acc) { n * acc })
}
```

Learn more about Type Annotations [here](https://gleam.run/book/tour/functions.html#type-annotations).


### Type Aliases

```gleam
type alias Name = String
type alias Age = Int
type alias Person = tuple(Name, Age)


fn steve() -> Person {
  tuple("Steve", 28)
}
```

Learn more about Type Aliases [here](https://gleam.run/book/tour/type-aliases.html).


### Erlang Interop
When in need, you can define an external function to call erlang code directly. The first string is always the 
module and the second string is always the function.
```gleam
external function random_bytes(num_bytes: Int) -> BitString =
  "string" "concat"
```

To learn more about Erlang Interop see: 
* [External Functions](https://gleam.run/book/tour/external-functions.html)
* [External Types](https://gleam.run/book/tour/external-types.html)



